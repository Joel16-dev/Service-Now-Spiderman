# -*- mode: python -*-

block_cipher = None


a = Analysis(['domain.py'],
             pathex=['C:\\Users\\Joel\\PycharmProjects\\web_crawler'],
             binaries=[],
             datas=[('C:\\Users\\Joel\\PycharmProjects\\web_crawler\\res\\banner.gif', 'res')],
             hiddenimports=['pandas._libs.tslibs.timedeltas'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Service-Now Spiderman',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True, icon='C:\\Users\\Joel\\PycharmProjects\\web_crawler\\res\\icon.ico')