from appJar import gui
import json
import os
import pandas
import requests
import sys
import time
from urllib.parse import urlparse


application_path = ''
banner = 'res/banner.gif'
dir_name: str = 'service-now'


def domain_init():
    if getattr(sys, 'frozen', False):
        application_path = sys._MEIPASS
    elif __file__:
        application_path = os.path.dirname(__file__)

    if not os.path.exists(dir_name):
        os.makedirs(dir_name)


# Get response code from URL:
def domain_get_response_code(url):
    try:
        r = requests.head(url)
        # print(url + ' returned the response code: ' + str(r.status_code))
        return r
    except requests.ConnectionError:
        return '<Invalid URL>'


# Get domain name (sample.com)
def domain_get_domain_name(url):
    results = domain_get_sub_domain(url).split('.')
    return results[-2] + '.' + results[-1]  # Gets second to last + . + last --> service-now + . + com


# Get sub domain name (sample.domain.com)
def domain_get_sub_domain(url):
    try:
        return urlparse(url).netloc  # return network location
    except ConnectionError:
        return ''


# Sort and remove clean up successful responses
def domain_cleanup_dump(input_path, output_path):
    sorted_list = list()
    with open(input_path) as f_in:
        for line in f_in:
            sorted_list.append(line.strip())

        sorted_list.sort()

    # If a previous output file existed, delete it and make a new one.
    if os.path.isfile(output_path):
        os.remove(output_path)

    with open(output_path, 'w') as f_out:
        for lines in sorted_list:
            f_out.write(lines + '\n')

    os.remove(input_path)


# Parses a JSON file
def domain_parse_json(file_path):
    start_time = time.time()

    with open(file_path) as json_file:
        json_data = json.load(json_file)

    file_path = os.path.join(dir_name, 'dump.txt')

    with open(file_path, 'a+') as result_file:
        for i in json_data:
            domain_name = ((domain_get_domain_name('https://' + i["site"])).split('.')[0])
            domain_url = 'http://' + domain_name + '.service-now.com/'
            result_string = str(domain_get_response_code(domain_url))
            data = domain_name + ', ' + domain_url + ': ' + result_string

            if not result_string == '<Invalid URL>':
                result_file.writelines(data + '\n')

            print(data)

    result_file.close()
    print("--- %s seconds ---" % ((time.time() - start_time) * 0.0166667))


# Parses a CSV file:
def domain_parse_csv(file_path):
    start_time = time.time()

    csv_data = pandas.read_csv(file_path)
    domain_list = csv_data.domain.tolist()

    file_path = os.path.join(dir_name, 'dump.txt')

    with open(file_path, 'a+') as result_file:
        for i in domain_list:
            domain_name = (i.split('.')[0])
            domain_url = 'http://' + domain_name + '.service-now.com/'
            result_string = str(domain_get_response_code(domain_url))
            data = domain_name + ', ' + domain_url + ': ' + result_string

            if not result_string == '<Invalid URL>':
                result_file.writelines(data + '\n')

            print(data)

    result_file.close()
    print("--- %s seconds ---" % ((time.time() - start_time) * 0.0166667))


# handle button events
def gui_handle_buttons(button):
    if button == "JSON":
        file_path = app.openBox(title='Select a .JSON file', dirName=None, fileTypes=None, asFile=False)

        if file_path:
            domain_parse_json(file_path)
            domain_cleanup_dump('service-now/dump.txt', 'service-now/service-now-json-list.txt')
            app.infoBox('JSON parsing complete:', 'Successfully redirected companies have been dumped into '
                                                  'service-now/service-now-list.txt', parent=None)
    elif button == "CSV":
        file_path = app.openBox(title='Select a .CSV file', dirName=None, fileTypes=None, asFile=False)

        if file_path:
            domain_parse_csv(file_path)
            domain_cleanup_dump('service-now/dump.txt', 'service-now/service-now-csv-list.txt')
            app.infoBox('JSON parsing complete:', 'Successfully redirected companies have been dumped into '
                                                  'service-now/service-now-list.txt', parent=None)
    else:
        domain_name = app.getEntry("Domain: ")
        domain_url = 'http://' + domain_name + '.service-now.com/'
        result_string = str(domain_get_response_code(domain_url))

        if not result_string == '<Invalid URL>':
            app.infoBox("Success:", domain_name + '\n' + domain_url + '\n' + result_string, parent=None)
        else:
            app.errorBox("Error:", domain_name + '\n' + result_string, parent=None)


# create a GUI variable called app and initialize domain
domain_init()
app = gui("Service Now: Spider-man", "640x360")
app.setBg("white")
app.setFont(18)

# add & configure widgets - widgets get a name, to help referencing them later
app.addLabel("title", "Service Now: Spider-man")
app.setLabelFg("title", "blue")
app.addImage("simple", os.path.join(application_path, banner), compound=None)

app.addLabelEntry("Domain: ")

# link the buttons to the function called press
app.addButtons(["Input company domain", "JSON", "CSV"], gui_handle_buttons)

# start the GUI
app.go()
