# Service-Now Spiderman

A tool that allows the user to check if a company domain is associated with service now.


# Features:
- Enter a domain without 'www' or '.com' to check if a single domain is associated with service now.
- Select a .JSON file with a list of companies. The .JSON file needs to be in a specific format as addressed below. (Please note: depending on the list this can take quite some time. (Up to 6 minutes for a list of 500 companies) You can check the progress in the console window.)
- Select a .CSV file with a list of company domains. The .CSV file requires one column with the name 'domain' and under it should be a list of domains example *walmart.com*


# Usage:

You can either run this tool through a command prompt, or the bundled .exe if you're a Windows user. The latest version of Python interpreter is recommended.

To use this via command line you need to install the folloing dependencies:

```pip install requests```
```pip install appJar```
```pip install pandas```

Then once you have installed the dependencies run ``python domain.py`` 


# JSON format:

The.JSON requires a specific format before it can be parsed: The **site** key and value is specifcally what the tool looks for, the other elements can be empty.
```
[
  {
    "name": "CVS Caremark",
    "site": "info.cvscaremark.com",
    "position": 11,
    "isSSL": false
  },
]
```


# Credits:

Stackoverflow: helping me fix a bunch of bugs/warnings that I encountered throughout the process of developing this tool.
